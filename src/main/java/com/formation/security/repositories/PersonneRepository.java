package com.formation.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.security.entities.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Long> {

}
