package com.formation.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.security.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	
	Role findByRole(String role);
}
