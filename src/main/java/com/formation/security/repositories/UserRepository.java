package com.formation.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.security.entities._User;

public interface UserRepository extends JpaRepository<_User, Long> {
	
	_User findByUsername(String email);

}
