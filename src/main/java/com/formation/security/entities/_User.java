package com.formation.security.entities;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class _User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUser;
	
	@Column(unique = true)
	private String username;
	
	private String password;
	
	private Boolean enabled;
	
	@ManyToMany
	@JoinTable(
			name ="user_role",
			joinColumns = @JoinColumn(name="idUser"),
			inverseJoinColumns = @JoinColumn(name="idRole")
	)
	private List<Role> roles;

}
