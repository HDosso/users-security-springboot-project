package com.formation.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsersSecurityApplication.class, args);
	}
	
	

}
