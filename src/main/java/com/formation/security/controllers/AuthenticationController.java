package com.formation.security.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.formation.security.entities._User;

@RestController
public class AuthenticationController {
	
	
	private UserDetailsService userDetailsService;
	
	@PostMapping("/login")
	public ResponseEntity<_User> authenticationPost( @RequestBody _User userAuth) 
	{
		_User userGuest = (_User) userDetailsService.loadUserByUsername(userAuth.getUsername());
		
		return ResponseEntity.ok().body(userGuest); 
	}
	 
	
	@GetMapping("/login")
	public ResponseEntity<?> authenticationGet() {
		return ResponseEntity.ok().body("Hello");
	}

}
