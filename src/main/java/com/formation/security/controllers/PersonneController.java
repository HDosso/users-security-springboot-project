package com.formation.security.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formation.security.entities.Personne;
import com.formation.security.repositories.PersonneRepository;

@RestController
@RequestMapping(path = "/personnes")
public class PersonneController {
	
	@Autowired
	PersonneRepository personneRepository;
	
	@GetMapping("/protected")
	public List<Personne> listePersonne() {
		return personneRepository.findAll();
	}
	
	@GetMapping("/public")
	public String listePublic() {
		return "La liste publique officielle n'est pas encore disponible";
	}

}
