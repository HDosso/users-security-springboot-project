package com.formation.security.utils;

public enum Civilite {
	HOMME, FEMME, AUTRE
}
