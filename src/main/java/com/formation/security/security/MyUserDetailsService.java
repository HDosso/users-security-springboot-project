package com.formation.security.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.formation.security.entities._User;
import com.formation.security.services.UserService;

public class MyUserDetailsService implements UserDetailsService{
	
	private UserService userService;
	
	public MyUserDetailsService(UserService userService) {
		
		this.userService = userService;
	}



	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		_User userApp = userService.findUserByUsername(username);
		
		if(userApp == null) {
			throw new UsernameNotFoundException("Merci de bien saisir votre nom utilisateur.");
		}
		
		List<GrantedAuthority> auths = new ArrayList<>();
		userApp.getRoles().forEach( role -> {
			GrantedAuthority authority = new SimpleGrantedAuthority(role.getRole());
			auths.add(authority);
		});
		return new User(userApp.getUsername(), userApp.getPassword(), auths);
	}

}
