package com.formation.security.services;

import com.formation.security.entities.Role;
import com.formation.security.entities._User;

public interface UserService {
	
	_User saveUser( _User user);
	_User findUserByUsername( String email);
	
	Role addRole( Role role);
	_User addRoleToUser(String email, String roleName);

}
