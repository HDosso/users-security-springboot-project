package com.formation.security;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.formation.security.entities.Personne;
import com.formation.security.entities.Role;
import com.formation.security.entities._User;
import com.formation.security.repositories.PersonneRepository;
import com.formation.security.services.UserService;
import com.formation.security.utils.Civilite;

@SpringBootTest
class UsersSecurityApplicationTests {
	
	@Autowired
	UserService userService;
	
	@Autowired
	PersonneRepository personneRepository;
	

	@Test
	void contextLoads() {
	}
	
	@Test
	void testSaveUser() {
		userService.saveUser(new _User(null, "h.dosso@gie-gck.com", "1234", true, null) );
		userService.saveUser(new _User(null, "kokora@gie-gck.com", "1234", true, null) );
		userService.saveUser(new _User(null, "yo@gie-gck.com", "1234", true, null) );
		userService.saveUser(new _User(null, "bakayoko@gie-gck.com", "1234", true, null) );
		userService.saveUser(new _User(null, "dohonakan@gie-gck.com", "1234", true, null) );
	}
	
	@Test
	void testSaveRole() {
		userService.addRole(new Role(null, "ADMIN"));
		userService.addRole(new Role(null, "USER"));
		
	}
	
	@Test
	void testSavePersonne() {
		/*
		 * personneRepository.save(new Personne(null, "Hamed", "DOSSO", "22/01/1942",
		 * Civilite.HOMME)); personneRepository.save(new Personne(null, "Jean",
		 * "KOKORA", "22/01/1942", Civilite.HOMME)); personneRepository.save(new
		 * Personne(null, "Naderge Nigbe", "YO", "22/01/1942", Civilite.FEMME));
		 * personneRepository.save(new Personne(null, "Test", "TEST", "22/01/1942",
		 * Civilite.AUTRE));
		 */
		

		personneRepository.save(new Personne(null, "Mamadou", "BAKAYOKO", "22/01/1942", Civilite.HOMME));
		personneRepository.save(new Personne(null, "Dohonakan", "BAMBA", "22/01/1942", Civilite.FEMME));
	}
	
	@Test
	void testAddRoleToUser() {
		/*
		 * userService.addRoleToUser("h.dosso@gie-gck.com", "ADMIN");
		 * userService.addRoleToUser("h.dosso@gie-gck.com", "USER");
		 * 
		 * userService.addRoleToUser("kokora@gie-gck.com", "USER");
		 * userService.addRoleToUser("yo@gie-gck.com", "USER");
		 */
		userService.addRoleToUser("bakayoko@gie-gck.com", "USER");
		userService.addRoleToUser("dohonakan@gie-gck.com", "USER");
	}

}
